package crud.app.cars.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import crud.app.cars.model.Car;
import crud.app.cars.model.Owner;

import static crud.app.cars.sqlite.DataBaseCreator.User.TABLE_NAME;
import static crud.app.cars.sqlite.DataBaseCreator.User.USER_CAR_ISSUE_YEAR;
import static crud.app.cars.sqlite.DataBaseCreator.User.USER_CAR_NAME;
import static crud.app.cars.sqlite.DataBaseCreator.User.USER_NAME;
import static crud.app.cars.sqlite.DataBaseCreator.User.USER_SURMANE;

/**
 * Created by Dmitriy Makovetskiy on 13.09.2017.
 * dmitriy.makovetskiyua@gmail.com
 */

public class DataBaseMaster {

    private SQLiteDatabase database;
    private DataBaseCreator dbCreator;

    private static DataBaseMaster instance;

    // initiate sql database
    private DataBaseMaster(Context context) {
        dbCreator = new DataBaseCreator(context);
        if (database == null || !database.isOpen()) {
            database = dbCreator.getWritableDatabase();
        }
    }

    // get instatce of database
    public static DataBaseMaster getInstance(Context context) {
        if (instance == null) {
            instance = new DataBaseMaster(context);
        }
        return instance;
    }

    public long insertUser(Owner owner, Car car) {

        //prepare content values
        ContentValues cv = new ContentValues();

        //put value to content values
        cv.put(USER_NAME, owner.getOwnerName());
        cv.put(USER_SURMANE, owner.getOwnerSurname());
        cv.put(USER_CAR_NAME, car.getCarName());
        cv.put(USER_CAR_ISSUE_YEAR, car.getCarIssueYear());

        //insert to database
        return database.insert(TABLE_NAME, null, cv);
    }

    public List<String> getUsers() {

        //query to get the data
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = database.rawQuery(query, null);

        List<String> list = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            //get the data
            String id = cursor.getString(cursor.getColumnIndex(DataBaseCreator.User._ID));
            String userName = cursor.getString(cursor.getColumnIndex(USER_NAME));
            String userSurname = cursor.getString(cursor.getColumnIndex(USER_SURMANE));
            String userCarName = cursor.getString(cursor.getColumnIndex(USER_CAR_NAME));
            String userCarIssueYear = cursor.getString(cursor.getColumnIndex(USER_CAR_ISSUE_YEAR));

            // add data to list
            list.add(id + " - " + userName + " - " + userSurname + " - " + userCarName + " - " + userCarIssueYear);
            cursor.moveToNext();
        }
        cursor.close();

        // receiving information in the list
        return list;
    }

    public List<String> afterDelete(String id) {

        // searching id and deleting information
        database.delete(TABLE_NAME, DataBaseCreator.User._ID + " =?", new String[]{id});

        // receiving updated information in the list
        return getUsers();
    }

    public List<String> updateUser(String id, String userName, String userSurname, String userCarName, String userCarIssueYear) {

        //prepare content values
        ContentValues cv = new ContentValues();

        //put updated value to content values
        cv.put(USER_NAME, userName);
        cv.put(USER_SURMANE, userSurname);
        cv.put(USER_CAR_NAME, userCarName);
        cv.put(USER_CAR_ISSUE_YEAR, userCarIssueYear);

        // updating row
        database.update(TABLE_NAME, cv, DataBaseCreator.User._ID + " =?", new String[]{id});

        // receiving updated information in the list
        return getUsers();
    }
}