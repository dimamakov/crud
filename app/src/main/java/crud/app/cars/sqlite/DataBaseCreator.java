package crud.app.cars.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by Dmitriy Makovetskiy on 13.09.2017.
 * dmitriy.makovetskiyua@gmail.com
 */

public class DataBaseCreator extends SQLiteOpenHelper {

    private static final String DB_NAME = "Crud.db";
    private static final int DB_VERSION = 1;

    public static class User implements BaseColumns {
        public static final String TABLE_NAME = "table_crud";
        public static final String USER_NAME = "user_name";
        public static final String USER_SURMANE = "user_surname";
        public static final String USER_CAR_NAME = "user_car_name";
        public static final String USER_CAR_ISSUE_YEAR = "user_car_issue_year";
    }

    static String SCRIPT_CREATE_TBL_MAIN = "CREATE TABLE " + User.TABLE_NAME + " (" +
            User._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            User.USER_NAME + " TEXT, " +
            User.USER_SURMANE + " TEXT, " +
            User.USER_CAR_NAME + " TEXT, " +
            User.USER_CAR_ISSUE_YEAR + " TEXT " +
            ");";

    public DataBaseCreator(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        // create table
        sqLiteDatabase.execSQL(SCRIPT_CREATE_TBL_MAIN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        //update table
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + User.TABLE_NAME);
    }
}
