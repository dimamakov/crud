package crud.app.cars.model;

/**
 * Created by Dmitriy Makovetskiy on 13.09.2017.
 * dmitriy.makovetskiyua@gmail.com
 */

public class Owner {

    private String ownerName;
    private String ownerSurname;

    public Owner(String ownerName, String ownerSurname) {
        this.ownerName = ownerName;
        this.ownerSurname = ownerSurname;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getOwnerSurname() {
        return ownerSurname;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void setOwnerSurname(String ownerSurname) {
        this.ownerSurname = ownerSurname;
    }
}
