package crud.app.cars.model;

/**
 * Created by Dmitriy Makovetskiy on 13.09.2017.
 * dmitriy.makovetskiyua@gmail.com
 */

public class Car {

    private String carName;
    private String carIssueYear;

    public Car(String carName, String carIssueYear) {
        this.carName = carName;
        this.carIssueYear = carIssueYear;
    }

    public String getCarName() {
        return carName;
    }

    public String getCarIssueYear() {
        return carIssueYear;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public void setCarIssueYear(String carIssueYear) {
        this.carIssueYear = carIssueYear;
    }
}
