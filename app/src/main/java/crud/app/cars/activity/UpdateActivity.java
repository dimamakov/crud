package crud.app.cars.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import crud.app.cars.R;
import crud.app.cars.adapter.UserAdapter;
import crud.app.cars.model.Car;
import crud.app.cars.model.Owner;
import crud.app.cars.sqlite.DataBaseMaster;

public class UpdateActivity extends AppCompatActivity implements UserAdapter.UserClickListener {

    // binding view with ButterKnife
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    DataBaseMaster dbMaster;
    List<String> users = new ArrayList<>();
    UserAdapter userAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work);
        ButterKnife.bind(this);

        // get instance of the db master and get data
        dbMaster = DataBaseMaster.getInstance(this);
        users.addAll(dbMaster.getUsers());

        // init RecyclerView
        LinearLayoutManager ll = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(ll);
        userAdapter = new UserAdapter(this, users, this);
        recyclerView.setAdapter(userAdapter);
    }

    @Override
    public void onClick(final String user) {

        // split the update string
        final String[] info;
        info = user.split(" - ");

        // get id of string
        final String id = info[0];

        // create custom view for pop up confirmation update
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.alert_dialog_create, null);
        final EditText userName = view.findViewById(R.id.edt_user_name);
        final EditText userSurname = view.findViewById(R.id.edt_user_surname);
        final EditText userCarName = view.findViewById(R.id.edt_car_name);
        final EditText userCarIssueYear = view.findViewById(R.id.edt_car_issue_year);

        // set updated information to edit text
        userName.setText(info[1]);
        userSurname.setText(info[2]);
        userCarName.setText(info[3]);
        userCarIssueYear.setText(info[4]);

        // pop up confirmation update
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Update user")
                .setMessage("Change one of these fields")
                .setView(view)
                .setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        // get all the parts of the string
                        String name = userName.getText().toString();
                        String surname = userSurname.getText().toString();
                        String carName = userCarName.getText().toString();
                        String carIssueYear = userCarIssueYear.getText().toString();

                        // clear the data
                        users.clear();

                        // save updated data and get new
                        users.addAll(dbMaster.updateUser(id, name, surname, carName, carIssueYear));

                        // refresh list
                        userAdapter.notifyDataSetChanged();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create()
                .show();

    }
}
