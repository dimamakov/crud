package crud.app.cars.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import crud.app.cars.R;
import crud.app.cars.adapter.UserAdapter;
import crud.app.cars.sqlite.DataBaseMaster;

public class WorkActivity extends AppCompatActivity implements UserAdapter.UserClickListener {

    // binding view with ButterKnife
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    DataBaseMaster dbMaster;
    List<String> users = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work);
        ButterKnife.bind(this);

        // get instance of the db master and get data
        dbMaster = DataBaseMaster.getInstance(this);
        users.addAll(dbMaster.getUsers());

        // init RecyclerView
        LinearLayoutManager ll = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(ll);
        UserAdapter userAdapter = new UserAdapter(this, users, this);
        recyclerView.setAdapter(userAdapter);
    }

    @Override
    public void onClick(String user) {

        // notice of reading
        Toast.makeText(this, "Go to another screen, here you can only read the information.", Toast.LENGTH_LONG).show();
    }
}