package crud.app.cars.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.OnClick;
import crud.app.cars.R;
import crud.app.cars.model.Car;
import crud.app.cars.model.Owner;
import crud.app.cars.sqlite.DataBaseMaster;

public class MainActivity extends AppCompatActivity {

    DataBaseMaster dbMaster;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        // get instance of the db master
        dbMaster = DataBaseMaster.getInstance(this);
    }

    @OnClick(R.id.bt_create)
    void onCreate() {

        // create custom view for pop up confirmation create
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.alert_dialog_create, null);
        final EditText userName = view.findViewById(R.id.edt_user_name);
        final EditText userSurname = view.findViewById(R.id.edt_user_surname);
        final EditText carName = view.findViewById(R.id.edt_car_name);
        final EditText carIssueYear = view.findViewById(R.id.edt_car_issue_year);

        // pop up confirmation create
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Create User")
                .setMessage("Fill the information")
                .setView(view)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        // save new user
                        dbMaster.insertUser(new Owner(userName.getText().toString(), userSurname.getText().toString()),
                                new Car(carName.getText().toString(), carIssueYear.getText().toString()));

                        // go read
                        startActivity(new Intent(MainActivity.this, WorkActivity.class));

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create()
                .show();
    }

    @OnClick(R.id.bt_read)
    void onRead() {
        startActivity(new Intent(MainActivity.this, WorkActivity.class));
    }

    @OnClick(R.id.bt_update)
    void onUpdate() {
        startActivity(new Intent(MainActivity.this, UpdateActivity.class));
    }

    @OnClick(R.id.bt_delete)
    void onDelete() {
        startActivity(new Intent(MainActivity.this, DeleteActivity.class));
    }
}
