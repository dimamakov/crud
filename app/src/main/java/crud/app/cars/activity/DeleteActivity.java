package crud.app.cars.activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import crud.app.cars.R;
import crud.app.cars.adapter.UserAdapter;
import crud.app.cars.sqlite.DataBaseMaster;

public class DeleteActivity extends AppCompatActivity implements UserAdapter.UserClickListener {

    // binding view with ButterKnife
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    DataBaseMaster dbMaster;
    List<String> users = new ArrayList<>();
    UserAdapter userAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work);
        ButterKnife.bind(this);

        // get instance of the db master and get data
        dbMaster = DataBaseMaster.getInstance(this);
        users.addAll(dbMaster.getUsers());

        // init RecyclerView
        LinearLayoutManager ll = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(ll);
        userAdapter = new UserAdapter(this, users, this);
        recyclerView.setAdapter(userAdapter);
    }

    @Override
    public void onClick(final String user) {

        // pop up confirmation delete
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete this user?")
                .setMessage(user)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        // split the delete string
                        String[] info;
                        info = user.split(" - ");

                        // clear the data
                        users.clear();

                        // delete and get the updated data
                        users.addAll(dbMaster.afterDelete(info[0]));

                        // refresh list
                        userAdapter.notifyDataSetChanged();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create()
                .show();
    }
}
