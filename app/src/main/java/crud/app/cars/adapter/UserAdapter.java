package crud.app.cars.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import crud.app.cars.R;

/**
 * Created by Dmitriy Makovetskiy on 13.09.2017.
 * dmitriy.makovetskiyua@gmail.com
 */

public class UserAdapter  extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    // interface for getting information
    public interface UserClickListener {
        void onClick(String user);
    }

    // interface for getting clicked position
    interface ItemClickListener {
        void onClick(int position);
    }

    private Context context;
    private List<String> users;
    private UserAdapter.UserClickListener userClickListener;
    private UserAdapter.ItemClickListener itemClickListener = new UserAdapter.ItemClickListener() {
        @Override
        public void onClick(int position) {
            userClickListener.onClick(users.get(position));
        }
    };

    public UserAdapter(Context context, List<String> users, UserAdapter.UserClickListener userClickListener) {
        this.context = context;
        this.users = users;
        this.userClickListener = userClickListener;
    }

    @Override
    public UserAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false);
        return new UserAdapter.ViewHolder(item, itemClickListener);
    }

    @Override
    public void onBindViewHolder(final UserAdapter.ViewHolder holder, int position) {

        // init field
        holder.user.setText(users.get(position));
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_info)
        TextView user;

        UserAdapter.ItemClickListener itemClickListener;

        public ViewHolder(View itemView, UserAdapter.ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.root)
        void onClick() {
            itemClickListener.onClick(getAdapterPosition());
        }
    }
}
